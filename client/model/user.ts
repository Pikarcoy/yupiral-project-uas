export class user {
    constructor(
        public user_id: string = "",
        public user_email: string = "",
        public user_password: string = "",
        public user_name: string = "",
        public user_nomor: string = "",
        public user_akses: string =""
    ){}
}