export class pertanian {
    constructor(
    public id_produk: string = "",
    public nama: string = "",
    public deskripsi: string = "",
    public harga: string = "",
    public berat: string = "",
    public images : string [] = [],
    public kode_item : string ="",
    public cat_id: string = ""
    ) {}
}