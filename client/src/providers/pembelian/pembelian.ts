import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs';
import { pembelian } from '../../../model/pembelian';

/*
  Generated class for the PembelianProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PembelianProvider {
  private pembelianlist: pembelian [] = [];

  constructor(public http: Http) {
    console.log('Hello PembelianProvider Provider');
  }

loadPembelian(){
  return this.http.get("http://localhost:8081/api/pembelian")
  .map((response: Response) => {
    let data = response.json();
    this.pembelianlist = data;
    return data;
  },
    (error) => console.log(error)
  );
}

tambahpembeli(data) {
  var url = "http://localhost:8081/api/pembelian";
  return this.http.post(url, data);
}

deletepembeli(id_pembeli) {
  var url = "http://localhost:8081/api/pembelian/" + id_pembeli;
  return this.http.delete(url);
}

updatepembeli(id_pembeli, data){
  var url = "http://localhost:8081/api/pembelian" + id_pembeli;
  return this.http.post(url, data);
}

}
