import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs';
import { pertanian } from '../../../model/pertanian';
/*
  Generated class for the PertanianProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PertanianProvider {
  private pertanianList: pertanian [] = [];

  constructor(public http: Http) {
    console.log('Hello PertanianProvider Provider');
  }

  loadPertanianBuah(){
    return this.http.get("http://localhost:8081/api/1/pertanian")
    .map((response: Response) => {
      let data = response.json();
      for (let elem of data) {
        elem.images = elem.images.split(",");
      }
      this.pertanianList = data;
      return data;
      },
        (error) => console.log(error)
      );
    }
    loadPertanianLauk(){
      return this.http.get("http://localhost:8081/api/2/pertanian")
      .map((response: Response) => {
        let data = response.json();
        for (let elem of data) {
          elem.images = elem.images.split(",");
        }
        this.pertanianList = data;
        return data;
        },
          (error) => console.log(error)
        );
      }

      loadPertanianRempah(){
        return this.http.get("http://localhost:8081/api/3/pertanian")
        .map((response: Response) => {
          let data = response.json();
          for (let elem of data) {
            elem.images = elem.images.split(",");
          }
          this.pertanianList = data;
          return data;
          },
            (error) => console.log(error)
          );
        }

      loadPertanianSayuran(){
        return this.http.get("http://localhost:8081/api/4/pertanian")
        .map((response: Response) => {
          let data = response.json();
          for (let elem of data) {
            elem.images = elem.images.split(",");
          }
          this.pertanianList = data;
          return data;
          },
            (error) => console.log(error)
          );
        }
}
