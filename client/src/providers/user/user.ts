import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs';
import { user } from '../../../model/user';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  private userlist: user[] = [];

  constructor(public http: Http) {
    console.log('Hello UserProvider Provider');
  }

loadUser(){
  return this.http.get("http://localhost:8081/api/user")
  .map((response: Response) => {
    let data = response.json();
    this.userlist = data;
    return data;
  },
  (error) => console.log(error)
  );
}

addUser(data) {
  var url = "http://localhost:8081/api/user";
  return this.http.post(url, data);
}

deleteUser(user_id){
  var url = "http://localhost:8081/api/user/" + user_id;
  return this.http.delete(url);
}

updateUser(user_id, data) {
  var url = "http://localhost:8081/api/user" + user_id;
  return this.http.post(url, data);
}

getUser2(user_email: String, user_password: String){
  return this.http.get("http://localhost:8081/api/login/"+user_email+"/"+user_password).map((response: Response) => {
    let data = response.json();
    this.userlist = data;
    return data;
  },
  (error) => console.log(error)
);
}

}
