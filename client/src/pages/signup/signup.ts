import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { user } from '../../../model/user';
import { UserProvider } from '../../providers/user/user';
import { WelcomePage } from '../welcome/welcome';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  private userlist: user[] = [];
  public user_email: string = "";
  public user_password: string = "";
  public user_name: string =""; 
  public user_nomor: string =""; 
  public user_akses: string = "";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public UserProvider:UserProvider) {
  }

  ngOnInit() {
    this.loadUser();
  }

loadUser(){
this.userlist = [];
this.UserProvider.loadUser()
.subscribe((result) => {
  console.log(result);
  this.userlist = result;
});
console.log(this.userlist);
};

addUser() {
this.navCtrl.push(WelcomePage)

console.log("addUser");
var data = {
  "user_email": this.user_email,
  "user_password": this.user_password,
  "user_name": this.user_name, 
  "user_nomor": this.user_nomor, 
  "user_akses": this.user_akses
}

console.log(data);
this.UserProvider.addUser(data).subscribe((result) => {
  console.log(result);
  this.loadUser();
});
}

  signup(){
    this.navCtrl.push(WelcomePage, {}, {animate: false});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}
