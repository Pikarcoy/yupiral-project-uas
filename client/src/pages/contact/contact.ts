import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { user } from '../../../model/user';
import { UserProvider } from '../../providers/user/user';
import { UpdateUserPage } from '../../pages/update-user/update-user';
import { pembelian } from '../../../model/pembelian';
import { PembelianProvider } from '../../providers/pembelian/pembelian';
import { UpdatePembeliPage } from '../../pages/update-pembeli/update-pembeli';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  private userlist: user[] = [];
  private pembelianlist: pembelian[] = [];
  constructor(
    public navCtrl: NavController,
    public UserProvider:UserProvider,
    public PembelianProvider: PembelianProvider,
    private modaltrl:ModalController
    ) {}

    ngOnInit() {
      this.loadUser();
      this.loadPembelian();
    }

deleteUser(user_id) {
  this.UserProvider.deleteUser(user_id).subscribe((result) => {
    console.log(result);
    this.loadUser();
  })
}

viewUser(data) {
  var modal = this.modaltrl.create(UpdateUserPage, data)
  modal.onDidDismiss(() => {
    this.loadUser();
  })
  modal.present();
}

loadUser(){
  this.userlist = [];
  this.UserProvider.loadUser()
  .subscribe((result) => {
    console.log(result);
    this.userlist = result;
  });
  console.log(this.userlist);
};

loadPembelian(){
  this.pembelianlist = [];
  this.PembelianProvider.loadPembelian()
  .subscribe((result) => {
    console.log(result);
    this.pembelianlist = result;
  });
  console.log(this.pembelianlist);
};

deletepembeli(id_pembeli){
  this.PembelianProvider.deletepembeli(id_pembeli).subscribe((result) => {
    console.log(result);
    this.loadPembelian();
  });
}

viewpembeli(data){
  var modal = this.modaltrl.create(UpdatePembeliPage, data)
  modal.onDidDismiss(() => {
    this.loadPembelian();
  })
  modal.present();
}

}
