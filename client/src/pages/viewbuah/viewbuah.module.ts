import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewbuahPage } from './viewbuah';

@NgModule({
  declarations: [
    ViewbuahPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewbuahPage),
  ],
})
export class ViewbuahPageModule {}
