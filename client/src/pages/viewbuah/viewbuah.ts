import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { MembeliPage } from '../membeli/membeli';

/**
 * Generated class for the ViewbuahPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewbuah',
  templateUrl: 'viewbuah.html',
})
export class ViewbuahPage {
  pertanianList: pertanian[] = [];
  categorySelected ="1";
  items:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider) {
    this.items = this.navParams.data;
  }
  
  ngOnInit(){
    this.loadPertanianBuah();
  }
  loadPertanianBuah(){
    this.pertanianList = [];
    this.PertanianProvider.loadPertanianBuah()
    .subscribe((result) => {
      console.log(result);
      this.pertanianList = result;
    });
  } 

  membeli(){
    this.navCtrl.push(MembeliPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewbuahPage');
  }

}