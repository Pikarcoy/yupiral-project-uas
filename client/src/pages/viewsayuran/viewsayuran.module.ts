import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewsayuranPage } from './viewsayuran';

@NgModule({
  declarations: [
    ViewsayuranPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewsayuranPage),
  ],
})
export class ViewsayuranPageModule {}
