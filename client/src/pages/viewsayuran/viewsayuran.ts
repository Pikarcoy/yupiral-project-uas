import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { MembeliPage } from '../membeli/membeli';

/**
 * Generated class for the ViewsayuranPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewsayuran',
  templateUrl: 'viewsayuran.html',
})
export class ViewsayuranPage {
  pertanianList: pertanian[] = [];
  categorySelected ="4";
  items:any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  
    ) {
    this.items = this.navParams.data;

    }
  
    ngOnInit(){
      this.loadPertanianSayuran();
    }
    loadPertanianSayuran(){
      this.pertanianList = [];
      this.PertanianProvider.loadPertanianSayuran()
      .subscribe((result) => {
        console.log(result);
        this.pertanianList = result;
      });
    } 

    membeli(){
      this.navCtrl.push(MembeliPage);
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewsayuranPage');
  }

}
