import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { ViewrempahPage } from '../viewrempah/viewrempah';
/**
 * Generated class for the RempahPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rempah',
  templateUrl: 'rempah.html',
})
export class RempahPage {
  pertanianList: pertanian[] = [];
  categorySelected ="4";
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  
    ) {
  }
  
  ngOnInit(){
    this.loadPertanianRempah();
  }
  loadPertanianRempah(){
    this.pertanianList = [];
    this.PertanianProvider.loadPertanianRempah()
    .subscribe((result) => {
      console.log(result);
      this.pertanianList = result;
    });
  } 

view(item){
  this.navCtrl.push(ViewrempahPage, item);
}
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad RempahPage');
  }

}