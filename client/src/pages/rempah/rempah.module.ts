import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RempahPage } from './rempah';

@NgModule({
  declarations: [
    RempahPage,
  ],
  imports: [
    IonicPageModule.forChild(RempahPage),
  ],
})
export class RempahPageModule {}
