import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SayuranPage } from './sayuran';

@NgModule({
  declarations: [
    SayuranPage,
  ],
  imports: [
    IonicPageModule.forChild(SayuranPage),
  ],
})
export class SayuranPageModule {}
