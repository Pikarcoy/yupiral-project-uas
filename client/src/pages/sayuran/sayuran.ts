import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { ViewsayuranPage } from '../viewsayuran/viewsayuran';
/**
 * Generated class for the SayuranPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sayuran',
  templateUrl: 'sayuran.html',
})
export class SayuranPage {
  pertanianList: pertanian[] = [];
  categorySelected ="4";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  
    ) {
    }
  
    ngOnInit(){
      this.loadPertanianSayuran();
    }
    loadPertanianSayuran(){
      this.pertanianList = [];
      this.PertanianProvider.loadPertanianSayuran()
      .subscribe((result) => {
        console.log(result);
        this.pertanianList = result;
      });
    } 
view(item){
  this.navCtrl.push(ViewsayuranPage, item);
}
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad SayuranPage');
  }

}
