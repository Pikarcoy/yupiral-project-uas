import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { BuahPage } from '../buah/buah';
import { LaukpaukPage } from '../laukpauk/laukpauk';
import { RempahPage } from '../rempah/rempah';
import { SayuranPage } from '../sayuran/sayuran';
import { WelcomePage } from '../welcome/welcome';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public app: App
    ) {
  }

logout(){
  console.log("Berhasil Logout");
  this.navCtrl.push(WelcomePage);
}

pindahHalamanBuahPage(){
  this.navCtrl.push(BuahPage);
}
pindahHalamanLaukpaukPage(){
  this.navCtrl.push(LaukpaukPage);
}
pindahHalamanRempahPage(){
    this.navCtrl.push(RempahPage);
}
pindahHalamanSayuranPage(){
  this.navCtrl.push(SayuranPage);
}
}