import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { MembeliPage } from '../membeli/membeli';

/**
 * Generated class for the ViewlaukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewlauk',
  templateUrl: 'viewlauk.html',
})
export class ViewlaukPage {
  pertanianList: pertanian[] = [];
  categorySelected ="2";
  items:any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  ) {
    this.items = this.navParams.data;
  }
  
  ngOnInit(){
    this.loadPertanianLauk();
  }
  loadPertanianLauk(){
    this.pertanianList = [];
    this.PertanianProvider.loadPertanianLauk()
    .subscribe((result) => {
      console.log(result);
      this.pertanianList = result;
    });
  } 

  membeli(){
    this.navCtrl.push(MembeliPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewlaukPage');
  }

}
