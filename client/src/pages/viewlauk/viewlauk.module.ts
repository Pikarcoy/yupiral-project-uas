import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewlaukPage } from './viewlauk';

@NgModule({
  declarations: [
    ViewlaukPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewlaukPage),
  ],
})
export class ViewlaukPageModule {}
