import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LaukpaukPage } from './laukpauk';

@NgModule({
  declarations: [
    LaukpaukPage,
  ],
  imports: [
    IonicPageModule.forChild(LaukpaukPage),
  ],
})
export class LaukpaukPageModule {}
