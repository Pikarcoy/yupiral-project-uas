import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { ViewlaukPage } from '../viewlauk/viewlauk';
/**
 * Generated class for the LaukpaukPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-laukpauk',
  templateUrl: 'laukpauk.html',
})
export class LaukpaukPage {

  pertanianList: pertanian[] = [];
  categorySelected ="2";
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  
    ) {
  }
  
  ngOnInit(){
    this.loadPertanianLauk();
  }
  loadPertanianLauk(){
    this.pertanianList = [];
    this.PertanianProvider.loadPertanianLauk()
    .subscribe((result) => {
      console.log(result);
      this.pertanianList = result;
    });
  } 

view(item){
  this.navCtrl.push(ViewlaukPage, item);
}
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad laukpaukPage');
  }

}
