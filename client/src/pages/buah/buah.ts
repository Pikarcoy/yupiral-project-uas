import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pertanian } from "../../../model/pertanian";
import { PertanianProvider } from "../../providers/pertanian/pertanian";
import { ViewbuahPage } from '../viewbuah/viewbuah';
/**
 * Generated class for the BuahPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buah',
  templateUrl: 'buah.html',
})
export class BuahPage {
  pertanianList: pertanian[] = [];
  categorySelected ="1";
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public PertanianProvider: PertanianProvider  
    ) {
  }
  
  ngOnInit(){
    this.loadPertanianBuah();
  }
  loadPertanianBuah(){
    this.pertanianList = [];
    this.PertanianProvider.loadPertanianBuah()
    .subscribe((result) => {
      console.log(result);
      this.pertanianList = result;
    });
  } 

view(item){
  this.navCtrl.push(ViewbuahPage, item);
}
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad BuahPage');
  }

}
