import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuahPage } from './buah';

@NgModule({
  declarations: [
    BuahPage,
  ],
  imports: [
    IonicPageModule.forChild(BuahPage),
  ],
})
export class BuahPageModule {}
