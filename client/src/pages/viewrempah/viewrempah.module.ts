import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewrempahPage } from './viewrempah';

@NgModule({
  declarations: [
    ViewrempahPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewrempahPage),
  ],
})
export class ViewrempahPageModule {}
