import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Item } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ContactPage } from '../contact/contact';

/**
 * Generated class for the UpdateUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-user',
  templateUrl: 'update-user.html',
})
export class UpdateUserPage {
  private user_id: string = "";
  private user_email: string = "";
  private user_password: string = "";
  private user_name: string = "";
  private user_nomor: string = "";
  private user_akses: string = "";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private userProvider: UserProvider) 
    { var data = navParams.data;
      this.user_id = data.user_id;
      this.user_email = data.user_email;
      this.user_password = data.user_password;
      this.user_name = data.user_name;
      this.user_nomor = data.user_nomor;
      this.user_akses = data.user_akses;
  } 

updateUser(){
  this.navCtrl.push(ContactPage, Item);
  var data = {
    "user_name" : this.user_name,
    "user_akses" : this.user_akses,
    "user_email" : this.user_email,
    "user_password" : this.user_password,
    "user_nomor" : this.user_nomor
  }

  this.userProvider.updateUser(this.user_id, data).subscribe((result) => {
    console.log(result);
    this.navCtrl.pop();
  })
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateUserPage');
  }

}
