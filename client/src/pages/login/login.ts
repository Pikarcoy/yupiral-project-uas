import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { user } from '../../../model/user';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../../pages/home/home';
import { ContactPage } from '../contact/contact';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public userlist: user [] = [];
  public user_email: string = "";
  public user_password: string = "";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public UserProvider: UserProvider) {
  }


login(){
  if(this.user_email == ""){
    const alert = this.alertCtrl.create({
      subTitle: 'Email Tidak Boleh Kosong !',
      buttons: ['OK']
     });
     alert.present();
  }

  else if(this.user_password == ""){
    const alert = this.alertCtrl.create({
      subTitle: 'Password Tidak Boleh Kosong !',
      buttons: ['OK']
     });
     alert.present();
  }

  else {
    this.userlist = [];
    this.UserProvider.getUser2(this.user_email, this.user_password).subscribe((result) => {
      if (result.length == 0){
        const alert = this.alertCtrl.create({
          subTitle: 'Email Atau Password Salah !',
          buttons: ['OK']
         });
         alert.present();
      }
      else{
        for (let login of result){
          if (login.user_akses == "admin"){
            console.log("login sebagai admin");
            this.navCtrl.setRoot(ContactPage, login.user_id);
          }
          else if (login.user_akses == "pembeli"){
            console.log("login sebagai pembeli");
            this.navCtrl.setRoot(HomePage, login.user_id);
          }
        }
      }
    });
  }
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
