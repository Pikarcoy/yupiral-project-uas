import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PembelianProvider } from '../../providers/pembelian/pembelian';

/**
 * Generated class for the UpdatePembeliPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-pembeli',
  templateUrl: 'update-pembeli.html',
})
export class UpdatePembeliPage {
  private id_pembeli: string = "";
  private namapembeli: string = "";
  private nomorpembeli: string = "";
  private jumlah: string = "";
  private alamatpembeli: string = "";


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private PembelianProvider: PembelianProvider) 
    {
    var data = navParams.data;
    this.id_pembeli = data.id_pembeli;
    this.namapembeli = data.namapembeli;
    this.nomorpembeli = data.nomorpembeli;
    this.jumlah = data.jumlah;
    this.alamatpembeli = data.alamatpembeli;
  }

updatepembeli(){
  var data = {
    "alamatpembeli": this.alamatpembeli,
    "jumlah": this.jumlah
  }

this.PembelianProvider.updatepembeli(this.id_pembeli, data).subscribe((result) => {
  console.log(result);
  this.navCtrl.pop();
})

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePembeliPage');
  }

}
