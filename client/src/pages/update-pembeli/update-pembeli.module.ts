import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdatePembeliPage } from './update-pembeli';

@NgModule({
  declarations: [
    UpdatePembeliPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdatePembeliPage),
  ],
})
export class UpdatePembeliPageModule {}
