import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembeliPage } from './membeli';

@NgModule({
  declarations: [
    MembeliPage,
  ],
  imports: [
    IonicPageModule.forChild(MembeliPage),
  ],
})
export class MembeliPageModule {}
