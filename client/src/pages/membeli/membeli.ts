import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { pembelian } from '../../../model/pembelian';
import { PembelianProvider } from '../../providers/pembelian/pembelian';
import { HomePage } from '../home/home';

/**
 * Generated class for the MembeliPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-membeli',
  templateUrl: 'membeli.html',
})
export class MembeliPage {
  private pembelianlist :pembelian[] = [];
  public namapembeli: string = "";
  public nomorpembeli: string = "";
  public jumlah: string = "";
  public alamatpembeli: string = "";
  items:any;

  constructor(public navCtrl: NavController,
  public navParams:NavParams,
  public PembelianProvider: PembelianProvider) {
  this.items = this.navParams.data;
}

ngOnInit(){
  this.loadPembelian();
}

loadPembelian(){
  this.pembelianlist = [];
  this.PembelianProvider.loadPembelian()
  .subscribe((result) => {
    console.log(result);
    this.pembelianlist = result;
  });
  console.log(this.pembelianlist);
};

tambahpembeli(){
  this.navCtrl.push(HomePage);
  console.log("tambahpembeli");
  var data = {
    "namapembeli": this.namapembeli,
    "nomorpembeli": this.nomorpembeli,
    "jumlah": this.jumlah,
    "alamatpembeli": this.alamatpembeli
  }

console.log(data);
this.PembelianProvider.tambahpembeli(data).subscribe((result) =>{
  console.log(result);
  this.loadPembelian();
});
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MembeliPage');
  }

}