import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BuahPage } from '../pages/buah/buah';
import { LaukpaukPage } from '../pages/laukpauk/laukpauk';
import { SayuranPage } from '../pages/sayuran/sayuran';
import { RempahPage } from '../pages/rempah/rempah';
import { PertanianProvider } from '../providers/pertanian/pertanian';
import { HttpModule } from '@angular/http';
import { ViewbuahPage } from '../pages/viewbuah/viewbuah';
import { ViewlaukPage } from '../pages/viewlauk/viewlauk';
import { ViewrempahPage } from '../pages/viewrempah/viewrempah';
import { ViewsayuranPage } from '../pages/viewsayuran/viewsayuran';
import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { UserProvider } from '../providers/user/user';
import { UpdateUserPage } from '../pages/update-user/update-user';
import { PembelianProvider } from '../providers/pembelian/pembelian';
import { MembeliPage } from '../pages/membeli/membeli';
import { UpdatePembeliPage } from '../pages/update-pembeli/update-pembeli';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    BuahPage,
    LaukpaukPage,
    SayuranPage,
    RempahPage,
    ViewbuahPage,
    ViewlaukPage,
    ViewrempahPage,
    ViewsayuranPage,
    WelcomePage,
    LoginPage,
    SignupPage,
    UpdateUserPage,
    MembeliPage,
    UpdatePembeliPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    BuahPage,
    LaukpaukPage,
    SayuranPage,
    RempahPage,
    ViewbuahPage,
    ViewlaukPage,
    ViewrempahPage,
    ViewsayuranPage,
    WelcomePage,
    LoginPage,
    SignupPage,
    UpdateUserPage,
    MembeliPage,
    UpdatePembeliPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PertanianProvider,
    UserProvider,
    PembelianProvider
  ]
})
export class AppModule {}
