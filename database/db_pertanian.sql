-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2018 at 09:03 AM
-- Server version: 5.5.40
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_pertanian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_produk`
--

CREATE TABLE IF NOT EXISTS `tb_produk` (
  `id_produk` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `images` longtext NOT NULL,
  `kode_item` varchar(45) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `nama`, `deskripsi`, `harga`, `berat`, `images`, `kode_item`, `cat_id`) VALUES
(1, 'Rambutan Kameng', 'Tampilan rambutan Kameng ini cukup menarik dengan warna kulit merah cerah dan ujung rambut kuning, rasa daging pada buah manis, sedikit berair dan mengelotok.\r\nKarakteristik bunga rambutan Kameng adalah warna kelopak hijau muda dengan warna mahkota putih. Sementara warna putik dan benang sari masing-masing adalah hijau muda dan krem, jumlah bunga pertandan sekitar 100 kuntum', 10000, 1, 'http://localhost:8081/img/buah_ac_001_0.jpg,http://localhost:8081/img/buah_ac_001_1.jpg,http://localhost:8081/img/buah_ac_001_2.jpg', 'buah_ac001', 1),
(2, 'Anggur Merah', 'Buah anggur merah mengandung senyawa  flavonoid dan antosianin, serta resveratol. Buah anggur juga mengandung vitamin C, vitamin E, vitamin A, vitamin B1, dan vitamin B2', 62000, 1, 'http://localhost:8081/img/buah_ac_002_0.jpg,http://localhost:8081/img/buah_ac_002_1.jpg,http://localhost:8081/img/buah_ac_002_2.jpg', 'buah_ac002', 1),
(3, 'Semangka', ': Semangka merah mengandung fosfor, magnesium, zat besi, selenium, folat, vitamin B6, C, E, dan K. Tak hanya itu, protein, niasin, dan serat juga bisa ditemukan dalam daging buah semangka merah, rasa yang manis dan menyegarkan', 3500, 1, 'http://localhost:8081/img/buah_ac003_0.jpg,http://localhost:8081/img/buah_ac003_1.jpg,http://localhost:8081/img/buah_ac003_2.jpg', 'buah_ac003', 1),
(4, 'Tahu', 'makanan yang dibuat dari endapan  perasan biji kedelai yang mengalami koagulasi. Tahu berasal dari Tiongkok, seperti halnya kecap, tauco, bakpau, dan bakso. Memilik warna yang Putih dan tebal', 17000, 1, 'http://localhost:8081/img/lauk_ac001_0.jpg,http://localhost:8081/img/lauk_ac001_1.jpg,http://localhost:8081/img/lauk_ac001_2.jpg', 'lauk_ac001', 2),
(5, 'Tempe', 'Tempe berwarna putih karena pertumbuhan miselia kapang yang merekatkan biji-biji kedelai sehingga terbentuk tekstur yang memadat. Degradasi komponen-komponen kedelai pada fermentasi membuat tempe memiliki rasa dan aroma khas. Berbeda dengan tahu, tempe terasa agak masam.', 17000, 1, 'http://localhost:8081/img/lauk_ac002_0.jpg,http://localhost:8081/img/lauk_ac002_1.jpg,http://localhost:8081/img/lauk_ac002_2.jpg', 'lauk_ac002', 2),
(6, 'Jengkol', 'Jengkol termasuk suku polong-polongan (Fabaceae). Buahnya berupa polong dan bentuknya gepeng berbelit membentuk spiral, berwarna lembayung tua. Biji buah berkulit ari tipis dengan warna coklat mengilap. Jengkol dapat menimbulkan bau tidak sedap pada urin setelah diolah dan diproses oleh pencernaan, terutama bila dimakan segar sebagai lalap.', 25000, 1, 'http://localhost:8081/img/lauk_ac003_0.jpg,http://localhost:8081/img/lauk_ac003_1.jpg,http://localhost:8081/img/lauk_ac003_2.jpg', 'lauk_ac003', 2),
(8, 'Bawang Putih', 'Bawang putih bagus dikonsumsi mentah atau dalam bentuk masakan. Sebagai bumbu utama masakan, bawang putih berperan untuk memberi rasa gurih dan sedikit pedas pada masakan. Bawang putih juga berfungsi sebagai penambah aroma dalam masakan. Bawang putih merupakan penguat rasa alami yang bebas dari bahan-bahan kimia. Oleh karena itu, sebagian besar masakan Indonesia menggunakan bawang putih sebagai bumbu utama. Contoh masakan berbumbu bawang putih adalah nasi goreng, roti bawang, stik bawang, dan sebagainya. Bawang putih memang cukup fleksibel dalam penggunaan untuk masakan ataupun kudapan.', 23000, 1, 'http://localhost:8081/img/rempah_ac002_0.jpg,http://localhost:8081/img/rempah_ac002_1.jpg,http://localhost:8081/img/rempah_ac002_2.jpg', 'rempah_ac002', 3),
(9, 'Bawang Merah', 'Bawang merah merupakan sejenis tumbuhan yang menjadi bumbu berbagai masakan di dunia, berasal dari Iran, Pakistan, dan pegunungan-pegunungan di sebelah utaranya, kemudian dibudidayakan di daerah dingin, sub-tropis maupun tropis. Umbi bawang dapat dimakan mentah, untuk bumbu masak, acar, obat tradisional, kulit umbinya dapat dijadikan zat pewarna dan daunnya dapat pula digunakan untuk campuran sayur.', 23000, 1, 'http://localhost:8081/img/rempah_ac003_0.jpg,http://localhost:8081/img/rempah_ac003_1.jpg,http://localhost:8081/img/rempah_ac003_2.jpg', 'rempah_ac003', 3),
(10, 'Tomat', 'Berbentuk agak bulat, berat sekitar 8 gram sampai dengan yang besar sekitar 180 gram, berwarna merah Tomat ternyata memiliki kandungan vitamin K, likopen, serat, vitamin A, dan kromium yang sangat bermanfaat bagi tubuh.', 12000, 1, 'http://localhost:8081/img/sayur_ac001_0.jpg,http://localhost:8081/img/sayur_ac001_1.jpg,http://localhost:8081/img/sayur_ac001_2.jpg', 'sayuran_ac001', 4),
(11, 'Brokoli', 'Brokoli ini merupakan jenis tanaman yang menyertai kubis, memiliki kandungan gizi yang sangat tinggi.', 13000, 1, 'http://localhost:8081/img/sayuran_ac002_0.jpg,http://localhost:8081/img/sayuran_ac002_1.jpg,http://localhost:8081/img/sayuran_ac002_2.jpg', 'sayuran_ac002', 4),
(12, 'Kentang', 'Bewarna Kuning ke orenan, mengandung karbohidrat, mengandung sejumlah vitamin dari A, B-kompleks, hingga C, hingga asam folat. Juga mineral, protein, karbohidrat, karotenoid, dan polifenol.', 18000, 1, 'http://localhost:8081/img/sayuran_ac003_0.jpg,http://localhost:8081/img/sayuran_ac003_1.jpg,http://localhost:8081/img/sayuran_ac003_2.jpg', 'sayuran_ac003', 4),
(13, 'Mangga Madu', 'kulitnya berwarna kuning cerah saat matang. Mangga ini sangat manis seperti madu, meski buahnya masih Mangga Madu, Sepintas mangga madu mirip gadung, kecuali keseluruhan berwarna putih, Buah mangga madu memiliki ukuran yang hampir sama dengan manalagi.', 13000, 1, 'http://localhost:8081/img/mangga1.jpg,http://localhost:8081/img/mangga2.jpg,http://localhost:8081/img/mangga3.jpg', 'buah_ac004', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_nomor` varchar(100) NOT NULL,
  `user_akses` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_email`, `user_password`, `user_name`, `user_nomor`, `user_akses`) VALUES
(13, 'pikar@gmail.com', 'pikar', 'pikar', '082288204492', 'admin'),
(14, 'alip@yahoo.com', 'alip', 'alip', '081288209909', 'penjual'),
(15, 'yudhy@yahoo.co.id', 'yudhy', 'yudhy', '087788224492', 'pembeli');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
